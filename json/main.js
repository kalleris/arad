(function() {
    window.onload = function () {
        let map = new BikeMap();
        map.initMap();
        fetchBikes(map);
        fetchWeather();
        //map.routeTest();
    };
})();

function fetchBikes(map) {
    let xmlhttp = new XMLHttpRequest();
    let url = "http://api.citybik.es/v2/networks/citybikes-helsinki?fields=stations";

    //TODO BEAUTIFY
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            var stations = JSON.parse(xmlhttp.responseText);
            myFunction(stations.network.stations, map);
        }
    };

    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}
function fetchWeather() {
    let xmlhttp = new XMLHttpRequest();
    let url = "http://api.openweathermap.org/data/2.5/weather?id=658225&appid=a8bef477e3c820e50e9dc96ed9fb716d";

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            //TODO
            var weather = JSON.parse(xmlhttp.responseText);
            let weatherhtml = document.getElementById("weather");
            weatherhtml.innerHTML = "Lämpötila: " + Math.round(weather.main.temp - 273.15) + " c.";
            let icon = document.createElement('img');
            icon.src = "http://openweathermap.org/img/w/" + weather.weather[0].icon + ".png";
            weatherhtml.appendChild(icon);
        }
    };

    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function myFunction(arr, map) {
    console.log(arr);
    var table = document.getElementById("fillarit");
    for(let i = 0; i < arr.length; i++) {
        var tr = table.insertRow();
        var asema = tr.insertCell(0);
        var paikkoja_vapaana = tr.insertCell(1);
        var pyoria = tr.insertCell(2);

        map.createStation(arr[i]);

        asema.innerHTML = arr[i].name;
        paikkoja_vapaana.innerHTML = arr[i].empty_slots;
        pyoria.innerHTML = arr[i].free_bikes;
    }
}

/*
xmlhttp.onreadystatechange = function() {
    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
        var myArr = JSON.parse(xmlhttp.responseText);
        myFunction(myArr.network.stations);
    }
};
xmlhttp.open("GET", url, true); // 4
xmlhttp.send();

var markers = [];
var chosenMarker = null;
function myFunction(arr) { // 5
    var table = document.getElementById("fillarit");
    var i;
    for(i = 0; i < arr.length; i++) {
        var tr = table.insertRow();
        var asema = tr.insertCell(0);
        var paikkoja_vapaana = tr.insertCell(1);
        var pyoria = tr.insertCell(2);

        var newMarker = new L.Marker([arr[i].latitude, arr[i].longitude]);
        newMarker.asema = arr[i].name;
        newMarker.paikkoja = arr[i].empty_slots;
        newMarker.pyoria = arr[i].free_bikes;
        markers.push(newMarker);
        //L.marker([arr[i].latitude, arr[i].longitude]).addTo(map);

        asema.innerHTML = arr[i].name;
        paikkoja_vapaana.innerHTML = arr[i].empty_slots;
        pyoria.innerHTML = arr[i].free_bikes;
    }

    for(let i = 0; i < markers.length; i++) {
        markers[i].on('click', test).addTo(map);
    }

    function test(e) {
        var chosen = document.getElementById("chosen");
        chosen.innerHTML = this.asema + " Vapaana: " +
            this.paikkoja + " Pyöriä: " +
            this.pyoria;
    }
}
*/