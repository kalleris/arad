function BikeMap() {
    this.map = null;
    this.stations = [];
    this.chosenStation = null;
    this.destinationStation = null;
    this.stationSwitch = true;

    this.initMap = function() {
        let element = document.getElementById('mapid');
        element.style = 'height:400px;';

        this.map = L.map(element);
        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo(this.map);

        this.centerMap('60.17076', '24.94046');
    };

    this.centerMap = function(lat, lng) {
        let target = L.latLng(lat.toString(), lng.toString());
        this.map.setView(target, 14);
    };

    this.updateChosen = function(e) {

        alert("hi. you clicked the marker at " + e.latlng + '\n' + this);
        /*
        let chosen = document.getElementById("chosen");
        chosen.innerHTML = this.asema + " Vapaana: " +
            this.paikkoja + " Pyöriä: " +
            this.pyoria;
           */
    };

    this.createStation = function(station) {
        station.marker = new L.Marker([station.latitude, station.longitude]);
        station.marker.on('click', (e) => {
            if(this.stationSwitch) {
                this.chosenStation = station;
                let from = document.getElementById("from");
                from.value = this.chosenStation.name;
                this.stationSwitch = false;
            }
            else {
                this.destinationStation = station;
                let to = document.getElementById("to");
                to.value = this.destinationStation.name;
                this.stationSwitch = true;
                this.routeTest();
                alert("calculating route");
            }
        }).addTo(this.map);
        this.stations.push(station);
    };

    this.routeTest = function() {
        let xmlhttp = new XMLHttpRequest();
        //let url = "../route.json";
        let url = "http://router.project-osrm.org/route/v1/driving/" +
            this.chosenStation.longitude + "," + this.chosenStation.latitude + ";" +
            this.destinationStation.longitude + "," + this.destinationStation.latitude +
            "?overview=false";
        let thatmap = this.map;

        //TODO BEAUTIFY
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                var waypoints = JSON.parse(xmlhttp.responseText).waypoints;

                L.Routing.control({
                    waypoints: [
                        L.latLng(waypoints[0].location[1], waypoints[0].location[0]),
                        L.latLng(waypoints[1].location[1], waypoints[1].location[0])
                    ],
                    routeWhileDragging: true
                }).addTo(thatmap);
            }
        };

        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }
}